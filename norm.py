absolute_min = 0.07999999999999999
absolute_max = 0.9328205128205128

max_values = [50, 50, 5, 120]
min_values = [1, 1, 0, 15]


def normalization(x, min, max):
    return round((x - min) / (max - min), 2)


def denormalization(y, min, max):
    coef = (max - min) / (absolute_max - absolute_min)
    return round((y - absolute_min) * coef + min, 1)
