import model
import inference_mamdani
import fuzzy_operators
from norm import *
from flask import Flask, render_template, request

app = Flask(__name__)
inference_mamdani.preprocessing(model.input_lvs, model.output_lv)


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        floor = int(request.form.get('floor'))
        metro = int(request.form.get('metro'))
        renovation = int(request.form.get('renovation'))
        area = int(request.form.get('area'))
        print(type(floor), floor)
        crisp = inference_mamdani.process(model.input_lvs, model.output_lv, model.rule_base,
                                          trst([floor, metro, renovation, area]))  # front output
        #
        for lv in model.input_lvs:
            fuzzy_operators.draw_lv(lv)
        fuzzy_operators.draw_lv(model.output_lv)
        result = denormalization(crisp[0], 0, 10), crisp[1]
        if result[0] > 7:
            return render_template('elite.html', result=result)
        elif result[0] > 5:
            return render_template('comfort.html', result=result)
        elif result[0] > 0:
            return render_template('low_price.html', result=result)
    return render_template('index.html')


def trst(crisp):
    norma_crisp = []
    for i in range(len(crisp)):
        norma_crisp.append(normalization(crisp[i], min_values[i], max_values[i]))
    return norma_crisp


if __name__ == '__main__':
    app.run()
